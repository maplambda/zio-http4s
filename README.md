# ZIO Effects with Http4s
This project is an example of running a [ZIO](https://zio.dev/) effect as a HTTP API. The example uses the [http4s](https://http4s.org/) dsl to create a program that will match a request to a response wrapped in a ```ZIO``` ```Task``` to return a ```Hello!``` message when run by the http4s server.

## Http4sDsl
Http4s requires an effect type representing a value to yield when matched against a request ```GET -> Root => Ok("hello!")```. Matching convenience functions are provided by Http4s dsl which requires an effect ```F[_]```.

To make the dsl for matching requests avaiable for ```ZIO``` programs we create ```ioz``` by providing the type parameter ```Task[A]``` alias for  ```ZIO[Any, Throwable, A]``` as the type parameter ```F[_]```.

## Response[Task]
A response is provided as a ZIO ```Task``` using the ```Ok``` method to return a ```String``` with a ```200 OK``` status.

## Program
The ```helloService``` program has the type ```Kleisli[Task, Request[Task], Response[Task]]```. When run via http4s this will receive the ```Request[Task]``` as a parameter and yield a ```Task[Response[Task[String]]``` the http4s server will execute to return the message, utlimately returning the ```String``` provided to the ```Ok``` method with a ```200 OK``` status.

## Cats interop
The http4s library is built on typeclasses provided by [cats](https://typelevel.org/cats/). Using this approach http4s is able to describe a program that can be run with any concrete effect type providing the needed typeclass instances. One concrete effect type providing the required typeclasses is ```IO``` from [cats-effect](https://typelevel.org/cats-effect/). The [zio.interop.catz](https://github.com/zio/interop-cats) package provides equivalent implementations for these typeclasses allowing execution of a ```ZIO``` program.